=begin

= Lab

Defines a lab. Essentially treated as a 2 dimensional
rack.

= COPYRIGHT

Copyright (c) 2013 Aaron Herting

Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

=end

require_relative "./return.rb"
require_relative "./postscript.rb"

include Postscript


class Lab
	attr_accessor :width, :length, :id, :contents, :name, :owner, :contact
	

=begin
Sets the rack to default values
=end
	def initialize
		# Set default height
		@width = 14
		@length = 15

		# Fill with empty values of each type
		@id = ""
		@name = ""
		@owner = ""
		@contact = ""
		@contents = []
	end


=begin
Adds an item to the rack at loc. 

loc is a the rack unit height off the floor, where
the bottom most rack unit is loc=0.

TODO: check to makesure item is actually a RackItem
=end
	def add_workstation item, x, y
		# Prevent an item from being placed above the rack
#		if 0 >= @height then
#			return false
#		end

		# Create a hash for the item
		content = {}

		# place in the item
		content[:item] = item
		
		# place in the location
		content[:x] = x
		content[:y] = y

		# Add the item to the contents array
		@contents.push content

		# Success
		return true
	end


=begin
Returns the postscript representation of the entire rack
including all rack items included in the rack.

TODO: add rack unit numbers to the side of the rack.
=end
	def to_ps
		# Create a return object
		to_return = Return.new

		# Print lab info below the lab image
		to_return.puts "0 -0.2 in moveto"
		to_return.puts "(#{@name}) show"
		to_return.puts "0 -0.4 in moveto"
		to_return.puts "(#{@id}) show"
		to_return.puts "0 -0.6 in moveto"
		to_return.puts "(#{@owner}) show"
		to_return.puts "0 -0.8 in moveto"
		to_return.puts "(#{@contact}) show"
		to_return.puts "0 0 moveto"

		# Build grid
		to_return.puts "0.8 setgray"
		@width.times do |x|
			to_return.puts "newpath"
			to_return.puts "#{x} slu 0 moveto"
			to_return.puts "#{x} slu #{@length} slu lineto"
			to_return.puts "stroke"
		end
		@length.times do |x|
			to_return.puts "newpath"
			to_return.puts "0 slu #{x} slu moveto"
			to_return.puts "#{@width} slu #{x} slu lineto"
			to_return.puts "stroke"
		end


		#Place items
		@contents.each do |item|
			to_return.puts "#{item[:x]} slu #{item[:y]} slu translate"
			to_return.puts item[:item].to_ps
			to_return.puts "-#{item[:x]} slu -#{item[:y]} slu translate"
		end


		

		return to_return.to_s
	end
end

