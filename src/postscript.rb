=begin

= Postscript

Defines some standard methods and some tricky Postscript
commands so they can be easily recalled elsewhere in the
program.

= COPYRIGHT

Copyright (c) 2013 Aaron Herting

Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

=end

require_relative "./return.rb"


module Postscript
=begin
Sets the scale for scaled unit definations.
=end
	@scale = 0.12


=begin
Returns the postscript command to do a reletive origin
translate to x, y
=end
	def translate x, y
		to_return = Return.new

		to_return.puts "#{x} #{y} translate"

		return to_return.to_s
	end


=begin
Returns the postscript command for setting the color of
the drawing in postscript.
=end
	def set_color r, g, b
		to_return = Return.new

		to_return.puts "#{r} #{g} #{b} setrgbcolor"

		return to_return.to_s
	end


=begin
Returns the "shebang" line for postscript files.
=end
	def header
		to_return = Return.new
		
		to_return.puts "%!PS"
		to_return.puts ""

		return to_return.to_s
	end


=begin
Returns the command for define a unit size.

unit - name of the unit
mul  - multiplication value
div  - division value

Source: wikipedia
=end
	def unit( unit, mul, div )
		to_return = Return.new

		to_return.puts "/#{unit} { #{mul} mul #{div} div } def"

		return to_return.to_s
	end


=begin
Allows postscript to know how long an inch is.

Source: wikipedia
=end
	def unit_in
		return unit( 'in', 72, 1 )
	end


=begin
Allows postscript to know how long a milimeter is.
=end
	def unit_mm 
		return unit( 'mm', 360, 127 )
	end


=begin
Allow postscript to know how long a rack unit is.
=end
	def unit_ru
		return unit( 'ru', '1.75 in' , 1 )
	end


=begin
Scales rack unit to a value that makes it easy for a full
rack to be placed on a piece of paper.
=end
	def scale_ru
		return unit( 'sru', "#{1.75 * @scale } in", 1 )
	end


=begin
Scales an inch to match the scale of the scaled rack unit.
=end
	def scale_in
		return unit( 'sin', 72 * @scale, 1 )
	end

	
	def scale_lu
		return unit( 'slu', "#{ 2.5 * @scale } in", 1 )
	end

=begin
Returns the Postscript commands to set the font typeface
and size.

typeface - the typeface to use
size     - the size of the font
=end
	def font( typeface, size )
		to_return = Return.new

		to_return.puts "/#{typeface} findfont"
		to_return.puts "#{size} scalefont"
		to_return.puts "setfont"

		return to_return.to_s
	end


=begin
Returns a default font. Currently "courier" with 12 pt.
=end
	def font_default
		return font( "Courier", 12 )
	end


=begin
Returns the commands to draw a screw hole. Don't quite know
why I wrote this....
=end
	def screw_hole x, y, scale
		to_return = Return.new
		
		to_return.puts ""
		to_return.puts "% Screw hole"
		to_return.puts "%   x     - #{x}"
		to_return.puts "%   y     - #{y}"
		to_return.puts "%   scale - #{scale}"	
		to_return.puts ""

		to_return.puts "newpath"
		to_return.puts "#{x} #{y} moveto"
		to_return.puts "#{-5 * scale} 0 rmoveto"
		to_return.puts "#{10 * scale} 0 rlineto"
		to_return.puts "stroke"
		
		to_return.puts "newpath"
		to_return.puts "#{x} #{y} moveto"
		to_return.puts "0 #{-5 * scale} rmoveto"
		to_return.puts "0 #{10 * scale} rlineto"
		to_return.puts "stroke"

		to_return.puts "newpath"
		to_return.puts "#{x} #{y} #{5 * scale} 0 365 arc"
		to_return.puts "stroke"

		return to_return.to_s
	end
end


