=begin

= BladeCenter

Defines a bladecenter object. A bladecenter (as defined
by this program) is a rack item that has 1-2 rows of
servers that belong to it. This servers are vertical 
rather then horrizontal.

= COPYRIGHT

Copyright (c) 2013 Aaron Herting

Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

=end



class BladeCenter < RackItem

# Array that holds internal blades
	attr :blades


	def initialize

# Empties the membership array
		@blades = []
	end



# Adds a blade to the BladeCenter
# 
# Parameters:
#   blade - The blade object being added.
#           [object]
#   loc   - The horrizontal location of the blade within
#           the blade center.
#           [int]
#           0 = left most postion
#   pos   - The vertical postion of the blade within the
#           blade center.
#           
#           :full   = A full blade.
#           :top    = A half blade on the top row.
#           :bottom = A half blade on the bottom row.
	def add_blade blade, loc, pos=:full
		case pos
			when :full
			blade.width = @height - 1

			when :bottom, :top
			blade.width = ( @height - 1 ) / 2.0
			
			else
			blade.width = @height - 1
		end

		blade.height = 1
		tmp = {}
		tmp[:loc] = loc
		tmp[:blade] = blade
		tmp[:pos] = pos
		@blades.push tmp
	end


# Generate the Postscript representation of this BladeCenter.
# Loops through the internal blade and adds them to the
# output in the proper location.
	def to_ps
		to_return = Return.new

		to_return.print super

		to_return.puts Postscript::translate( "1 sin", "1 sru" )
		to_return.puts '90 rotate'

		@blades.each do |blade|
			to_return.puts Postscript::translate( 0, "-#{blade[:loc] + 1} sru" )

			if blade[:pos] == :top then
				to_return.puts Postscript::translate( "#{( @height - 1 ) / 2.0} sru", 0)
			end	

			to_return.puts blade[:blade].to_ps
			
			if blade[:pos] == :top then
				to_return.puts Postscript::translate( "-#{( @height - 1 ) / 2.0} sru", 0)
			end	

			to_return.puts Postscript::translate( 0, "#{blade[:loc] + 1} sru" )
		end


		to_return.puts '-90 rotate'
		to_return.puts Postscript::translate( "-1 sin", "-1 sru" )
	end

end

