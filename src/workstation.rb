=begin

= Workstation

Essentially a rack item but is ment for 2 dimensional
space. Does not extend RackItem.


= COPYRIGHT

Copyright (c) 2013 Aaron Herting

Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

=end


require_relative "./postscript.rb"
require_relative "./return.rb"
require_relative './zabbix.rb'

include Postscript

class Workstation
	attr_accessor :length, :width, :name
	attr :r, :g, :b

	@@BLACK = { :r => 0 , :g => 0 , :b => 0 }


=begin
Sets the default values for the RackItem
=end
	def initialize
		@length = 1
		@width = 1
		@name = ""	
		@r = 0.7
		@g = 0.7
		@b = 0.7
	end


=begin
Return the postscript representation of the server
=end
	def to_ps
		to_return = Return.new

		to_return.puts ps_comment
		to_return.puts ps_fill
		to_return.puts ps_outline
		to_return.puts ps_name

		return to_return.to_s
	end

=begin
Add zabbix support to this Workstation
=end
	def add_zabbix
		extend Zabbix
		init
	end

=begin
Sets the color of the workstation
=end
	def set_color r, g, b
		@r = r
		@g = g
		@b = b
	end



	protected


=begin
Returns the path of the outline of the RackItem.
Allows for both ps_outline and ps_fill to have the
same outline and not have to repeat code.
=end
	def ps_path
		to_return = Return.new
		
		to_return.puts "newpath"
		to_return.puts "0 0 moveto"
		to_return.puts "0 slu #{@length} slu lineto"
		to_return.puts "#{@width} slu #{@length} slu lineto"
		to_return.puts "#{@width} slu 0 slu lineto"
		to_return.puts "0 0 lineto"
		
		return to_return.to_s
	end


=begin
Returns the ps_path along with the stroke command
=end
	def ps_outline
		to_return = Return.new
		
		to_return.print ps_path
		to_return.puts "0 setgray"
		to_return.puts "stroke"
		
		return to_return.to_s
	end
		

=begin
Returns the postscript commands to print the
name on the bottom RU of the RackItem.
=end
	def ps_name
		to_return = Return.new

		to_return.puts "0.2 sin 0.2 sin moveto"
		to_return.puts "0 setgray"
		to_return.puts "(#{@name}) show"

		return to_return
	end


=begin
Returns a postscript comment so the postscript 
source file can be read by a human.
=end
	def ps_comment
		to_return = Return.new
		
		to_return.puts
		to_return.puts "% Workstation"
		to_return.puts "%   name   - #{@name}"
		to_return.puts "%   width  - #{@width}"
		to_return.puts "%   length - #{@length}"

		return to_return.to_s
	end


=begin
Returns ps_path and a fill command.
Greyscale
=end
	def ps_fill
		to_return = Return.new

		to_return.print ps_path
		to_return.puts "#{@r} #{@g} #{@b} setrgbcolor"
		to_return.puts "fill"

		return to_return.to_s

	end
end



