=begin

= ColorRackItem

Allows the user to define a rack item the has a color.
In addition to height and name, ColorRackItem also holds
values for red, green, and blue. These can only be set
via the set_color method.

= COPYRIGHT

Copyright (c) 2013 Aaron Herting

Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

=end

require_relative './rackitem.rb'
require_relative './postscript.rb'
require_relative './zabbix.rb'

include Postscript

class ColorRackItem < RackItem
	attr :r, :g, :b

	@@BLACK = { :r => 0 , :g => 0 , :b => 0 }


=begin
Sets the default color to black. This probably should be
changed as the text is black too.

TODO: Change default color
=end
	def initialize
		@r = 0.7
		@g = 0.7
		@b = 0.7
	end


=begin
Allows the user to set the color of the rack item.

TODO: verify user supplied values
TODO: allow for html colors and convert
=end
	def set_color r, g, b
		@r = r
		@g = g
		@b = b
	end


=begin
Allows user to add Zabbix support

TODO: Remove the duplication of this in ZabbixRackItem
TODO: Check to see if the init call is needed or not
=end
	def add_zabbix
		extend Zabbix
		init
	end



	protected

=begin
Add color info to the postscript comment.
=end
	def ps_comment
		to_return = Return.new

		to_return.print super
		to_return.puts "%   r - #{@r}"
		to_return.puts "%   g - #{@g}"
		to_return.puts "%   b - #{@b}"
		
		return to_return.to_s
	end

	
=begin
Redefine fill to set the fill color to the user
defined color.
=end
	def ps_fill
		to_return = Return.new

		to_return.print ps_path
		to_return.puts "#{@r} #{@g} #{@b} setrgbcolor"
		to_return.puts "fill"

		return to_return
	end
end


