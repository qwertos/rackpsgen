#!/usr/bin/ruby

=begin



= COPYRIGHT

Copyright (c) 2013 Aaron Herting

Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

=end




$SINGLE_RACK = ARGV[0]

require_relative "./return.rb"
require_relative "./postscript.rb"
require_relative "./rack.rb"
require_relative "./lab.rb"
require_relative "./workstation.rb"
require_relative "./rackitem.rb"
require_relative "./colorrackitem.rb"
require_relative "./zabbixrackitem.rb"
require_relative "./bladecenter.rb"
require_relative "./blade.rb"
require_relative "./config.rb"
#require_relative "./zabbix.rb"

if $USE_ZABBIX then
	require 'rubygems'
	require 'rubix'
end

include Postscript


# Make sure the entire file gets loaded prior to the
# execuation of the program.
def main

	# Prints out the shebang line for Postscript files
	puts Postscript::header

	# Adds the defination for the unit inch
	puts Postscript::unit_in

	# Adds the defination for the unit milimeter
	puts Postscript::unit_mm

	# Adds the defination for a scaled rack unit
	puts Postscript::scale_ru

	# Adds the defination for a scaled inch
	puts Postscript::scale_in
	
	# Adds the defination for a scaled lab unit
	puts Postscript::scale_lu

	# Sets the font
	#   Default is: Courier with 12 pt font
	puts Postscript::font_default
	
	
	# Outputs the generated racks
	#
	# As the output is 2 racks per page, the odd number
	#	and even number racks are processed differently.
	# odd gets flipped for every rack.
	odd = true
	$RACKS.each do |rack|
		if rack.class == Rack then
			if odd then
				# Sets the origin of the document 1 inch by 1 inch
				#	from the bottom left corner of the page.
				puts Postscript::translate( "1 in", "1 in" )
					
	
				# Use the rack's to_ps method to create the postscript
				# representation of the rack and print to stdout
				puts rack.to_ps
			else
				# Sets the origin of the document 4.25 inches from
				# the left side of the document and 1 inch from the
				# bottom. Notice, This is reletive to the last origin
				#	of the document.
				puts Postscript::translate( "3.25 in", 0 )
				
				# Use the rack's to_ps method to create the postscript
				# representation of the rack and print to stdout
				puts rack.to_ps
				
				# Tells the postscript interpereter that a page has ended.
				# This also sets the origin of coordnates back to the 
				# bottom left corner of the page. 
				puts "showpage"
			end
	
			# Flip the odd boolean value so the next rack gets placed
			# in the opposite location.
			odd = !odd
		elsif rack.class == Lab then
			if !odd then
				puts "showpage"
			end
			odd = true
			# Sets the origin of the document 1 inch by 1 inch
			#	from the bottom left corner of the page.
			puts Postscript::translate( "1 in", "1 in" )
				

			# Use the rack's to_ps method to create the postscript
			# representation of the rack and print to stdout
			puts rack.to_ps
			
		puts "showpage"
		end
	end

	# make sure that the has a show page at the end
	if !odd then
		puts "showpage"
	end

end


# Execute the program
main



