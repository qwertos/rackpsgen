=begin

= Blade

Defines a Blade object. Ment to be added to to a BladeCenter.



= COPYRIGHT

Copyright (c) 2013 Aaron Herting

Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

=end



require_relative './zabbix.rb'


class Blade < ColorRackItem

# Width variable. Yes this can be edited directly. This is
# not recommened as adding this to the BladeCenter will
# overwrite this value.
	attr_accessor :width


	def initialize
		super
		@width = 0
	end


# Adds Zabbix support to the Blade. 	
	def add_zabbix
		extend Zabbix
	end


	protected
	
# Redefine ps_name for better readability and more space
# for names.
	def ps_name
		to_return = Return.new

		to_return.puts "0.2 sin 0.2 sin moveto"
		to_return.puts "0 setgray"
		to_return.puts "(#{@name}) show"

		return to_return
	end


# Redefine ps_path as a Blade has a very different shape
# then the default ps_path.
	def ps_path
		to_return = Return.new

		to_return.puts "newpath"
		to_return.puts "0 0 moveto"
		to_return.puts "#{@width} sru 0 lineto"
		to_return.puts "#{@width} sru #{@height} sru lineto"
		to_return.puts "0 #{@height} sru lineto"
		to_return.puts "0 0 lineto"

		return to_return.to_s
	end
end




