=begin

= ZabbixRackItem

Defines a ColorRackItem that can call a zabbix server and
set its color based on the status of rack item.

zabbix_hostname - the hostname of the of the rack item that
                  is known by the zabbix server

= COPYRIGHT

Copyright (c) 2013 Aaron Herting

Permission is hereby granted, free of charge, to any
person obtaining a copy of this software and associated
documentation files (the "Software"), to deal in the
Software without restriction, including without
limitation the rights to use, copy, modify, merge,
publish, distribute, sublicense, and/or sell copies of
the Software, and to permit persons to whom the Software
is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice
shall be included in all copies or substantial portions
of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
IN THE SOFTWARE.

=end


if $USE_ZABBIX then
 require 'rubygems'
 require 'rubix'
end

require_relative './colorrackitem.rb'




module Zabbix

=begin
Defines the default colors for ZabbixRackItems.
=end
	@@ZABBIX_DEFAULT_COLORS = {
		# Some sort of blueish thing
		:not_found => {
			:r => 0.5,
			:g => 0.5,
			:b => 1
		},
		# A light grey
		:not_tracked => {
			:r => 0.8,
			:g => 0.8,
			:b => 0.8
		},
		# Green
		:good => {
			:r => 0,
			:g => 1,
			:b => 0
		},
		# Red
		:error => {
			:r => 1,
			:g => 0,
			:b => 0
		},
		# Yellow
		:warning => {
			:r => 1,
			:g => 1,
			:b => 0
		}
	}


=begin
The actual colors for ZabbixRackItems is defined here
=end
	@@zabbix_colors = nil


=begin
Builds the ZabbixRackItem
=end
	def initialize
		# Checks to see if the user has defined
		# colors in a config file. If not, set to default
		# else, use user's color choice.
		if $ZABBIX_COLORS == nil then
			@@zabbix_colors = @@ZABBIX_DEFAULT_COLORS
		else
			@@zabbix_colors = $ZABBIX_COLORS
		end
		
		# Sets the default color to a not tracked color
		set_color :not_tracked
	end

	def init
		# Checks to see if the user has defined
		# colors in a config file. If not, set to default
		# else, use user's color choice.
		if $ZABBIX_COLORS == nil then
			@@zabbix_colors = @@ZABBIX_DEFAULT_COLORS
		else
			@@zabbix_colors = $ZABBIX_COLORS
		end
		
		# Sets the default color to a not tracked color
		set_color :not_tracked
	end


	def zabbix_hostname= name
		@zabbix_hostname = name
	end

=begin
Refresh the status and color of the rack item
=end
	def refresh
		status = zabbix_status
		set_color status
		return status
	end
	

=begin
Returns the postscript value of the ZabbixRackItem.
Overridden to make sure the color is refreshed before
returning the postscript RackItem.
=end
	def to_ps
		refresh
		super
	end



	protected

=begin
Adds the zabbix_hostname and the status of the server
to the postscript comment.
=end
	def ps_comment
		to_return = Return.new

		to_return.print super
		to_return.puts "%   zabbix_hostname - #{@zabbix_hostname}"

		return to_return
	end



	private
	
=begin
Acquires and returns the status of the server based
on values the user sets in $ZABBIX_TRIGGER_LISTEN
=end
	def zabbix_status
		unless $USE_ZABBIX then
			return :not_tracked
		end

		# If we have not yet connected to the zabbix server
		# connect now.
		if !Rubix.connected? then
			Rubix.connect( $ZABBIX_API_URL, $ZABBIX_API_USERNAME, $ZABBIX_API_PASSWORD )
		end

		# Get the trigger information for the current 
		#	Rack Item.
		resp = Rubix.connection.request( 'host.get', 'filter' => { 'host' => @zabbix_hostname } , 'selectTriggers' => 'extend' )
		
		# Process the data returned
		if resp.has_data? then
			# If the server exists

			# Iterate through the triggers looking for the
			# triggers the user has defined. Return the
			# status given defined by the user.
			resp.result[0]['triggers'].each do |trigger|
				if $ZABBIX_TRIGGER_LISTEN.has_key? trigger['description'] then
					if trigger['value'] != '0' then
						return $ZABBIX_TRIGGER_LISTEN[trigger['description']]
					end
				end
			end

			# Return good if no issues have been found.
			return :good
		elsif resp.success? then
			# If connection was a success but never found an
			# item in zabbix with the hostname defined here.
			return :not_found
		else 
			#	If connection failed, put the error message in the
			# postscript file as a comment
			puts "% #{resp.error_message}"
			return :not_tracked
		end
	end


=begin
Sets the color of the RackItem based on the status given
to the method
=end
	def set_color status
		@r = @@zabbix_colors[status][:r]
		@g = @@zabbix_colors[status][:g]
		@b = @@zabbix_colors[status][:b]
	end
end


