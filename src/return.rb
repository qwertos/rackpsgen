#!/usr/bin/ruby



class Return
	attr_reader :data

	def initialize
		@data = ""
	end

	def puts line=""
		self.print line.to_s + "\n"
	end

	def print data=""
		@data += data
	end

	def to_s
		return data
	end
end
		

