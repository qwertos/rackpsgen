<!-- vim: set noexpandtab tabstop=4 shiftwidth=4 : -->

#Rack inventory and layout postscript generator

Release: v0.2.2

##New feature in this release

+	Bugfix

+	Should no longer require rubix to be installed
+	Easy to change all zabbix items to greyscale for printing on a greyscale printer
+	Easy to visualize when no zabbix server is available to auth against
+	**NOTICE**: Looking to remove ZabbixRackItem in the future and replacewith ColorRackItem.add_zabbix


##Dependencies

+	Ruby
+	Rubix

###Suggested tools

+	GhostScript


###Tested with (originally developed on)
	
	$ ruby --version	
	ruby 1.9.3p194 (2012-04-20 revision 35410) [x86_64-linux]
	
	$ gem --version
	1.8.23
	
	$ gem dependency rubix
	Gem rubix-0.5.14
	  configliere (>= 0.4.16)
	  multi_json (>= 0)
	  multipart-post (>= 0)
	  mysql2 (>= 0, development)
	  oj (>= 0, development)
	  rake (>= 0, development)
	  rspec (>= 0, development)

	$ cat /etc/issue
	Ubuntu 12.10 \n \l

	$ gs --version
	9.06

###Now developed on

	$ ruby --version
	ruby 2.0.0p247 (2013-06-27 revision 41674) [x86_64-linux]

	$ gem --version
	2.0.13

	$ gem dependency rubix
	Gem rubix-0.5.14
	  configliere (>= 0.4.16)
	  multi_json (>= 0)
	  multipart-post (>= 0)
	  mysql2 (>= 0, development)
	  oj (>= 0, development)
	  rake (>= 0, development)
	  rspec (>= 0, development)

	$ cat /etc/issue
	Fedora release 19 (Schrödinger’s Cat)
	Kernel \r on an \m (\l)

	$ gs --version
	9.10

	$ uname -a
	Linux [redacted] 3.11.7-200.fc19.x86_64 #1 SMP Mon Nov 4 14:09:03 UTC 2013 x86_64 x86_64 x86_64 GNU/Linux


###Install dependencies with Ubuntu (YMMV)

	$ sudo apt-get install ruby rubygems
	$ sudo gem install rubix


##Configure

Look at config.rb.example for more information



##Use

Once the configure file is setup and all dependancies
have been met, run the file `psgen.rb` from the source
directory.


##Todo

+	Command Line arguments
+	Make the program location agnostic




